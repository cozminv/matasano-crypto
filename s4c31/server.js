'use strict';

var crypto = require('crypto');
var express = require('express');
var app = express();

var SECRET = "ULTRA SECRET KEY";

function insecure_compare(s1, s2, callback) {
	var i = 0;

	function cc() {
		var c1 = s1.charAt(i);
		var c2 = s2.charAt(i);

		if (!c1 && !c2)
			return callback(true);

		if (!c1 || !c2)
			return callback(false);

		if (c1 != c2)
			return callback(false);

		if (c1 == c2) {
			setTimeout(cc, 50);
			i++;
		}
	}

	cc();
}

app.get('/test', function(req, res) {
	var file = req.query.file;
	var sign = req.query.signature;

	var hmac = crypto.createHmac('sha1', SECRET).update(file).digest('hex');
	console.log(hmac);

	insecure_compare(sign, hmac, function(valid) {
		if (valid)
			res.status(200).send();
		else
			res.status(500).send();
	});
});

var server = app.listen(9000, function() {

	var host = server.address().address;
	var port = server.address().port;

	console.log('Example app listening at http://%s:%s', host, port);

});
