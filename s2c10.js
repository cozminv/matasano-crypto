'use strict';

var aes = require("./aes");

module.exports = {
	AES_CBC_encrypt : AES_CBC_encrypt,
	AES_CBC_decrypt : AES_CBC_decrypt
};

function AES_CBC_encrypt(data, key, iv) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof iv !== 'string' && !(iv instanceof Buffer))
		throw new TypeError("The iv should be a string or a Buffer.");

	if (data.length % 16 != 0)
		throw new TypeError("Invalid data length.");

	if (typeof data === 'string') {
		data = new Buffer(data);
	}
	data = Array.prototype.slice.call(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	if (typeof iv === 'string') {
		iv = new Buffer(iv);
	}
	iv = Array.prototype.slice.call(iv);

	if (key.length != iv.length)
		throw new TypeError("Mismatching key and iv lengths.");

	switch (key.length) {
	case 16:
		break;
	case 24:
	case 32:
		throw new TypeError(key.length + " bit key support not implemented.");
	default:
		throw new TypeError("AES only works for a key size of 128, 192, or 256 bits.");
	}

	if (data.length % key.length != 0)
		throw new TypeError("Mismatching key and data lengths.");

	var res = [];
	while (data.length) {
		var plaintext = data.splice(0, key.length);
		plaintext.forEach(function(byte, index) {
			plaintext[index] ^= iv[index];
		});
		plaintext = new Buffer(plaintext);
		var ciphertext = aes.encrypt(plaintext, key);
		iv = ciphertext;
		res.splice.bind(res, 0, 0).apply(null, ciphertext); // JS magic
	}

	res = new Buffer(res);
	return res;
}

function AES_CBC_decrypt(data, key, iv) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof iv !== 'string' && !(iv instanceof Buffer))
		throw new TypeError("The iv should be a string or a Buffer.");

	if (data.length % 16 != 0)
		throw new TypeError("Invalid data length.");

	if (typeof data === 'string') {
		data = new Buffer(data);
	}
	data = Array.prototype.slice.call(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	if (typeof iv === 'string') {
		iv = new Buffer(iv);
	}
	iv = Array.prototype.slice.call(iv);

	if (key.length != iv.length)
		throw new TypeError("Mismatching key and iv lengths.");

	switch (key.length) {
	case 16:
		break;
	case 24:
	case 32:
		throw new TypeError(key.length + " bit key support not implemented.");
	default:
		throw new TypeError("AES only works for a key size of 128, 192, or 256 bits.");
	}

	if (data.length % key.length != 0)
		throw new TypeError("Mismatching key and data lengths.");

	var res = [];
	while (data.length) {
		var ciphertext = data.splice(0, key.length);
		var plaintext = aes.decrypt(new Buffer(ciphertext), key);
		plaintext = Array.prototype.slice.call(plaintext);
		plaintext.forEach(function(byte, index) {
			plaintext[index] ^= iv[index];
		});
		iv = ciphertext;
		res.splice.bind(res, 0, 0).apply(null, plaintext); // JS magic
	}

	res = new Buffer(res);
	return res;
}
