'use strict';

var aes = require("./aes");

module.exports = {
	AES_CTR : AES_CTR
};

function AES_CTR(data, key) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof data === 'string') {
		data = new Buffer(data);
	}
	data = Array.prototype.slice.call(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	switch (key.length) {
	case 16:
		break;
	case 24:
	case 32:
		throw new TypeError(key.length + " bit key support not implemented.");
	default:
		throw new TypeError("AES only works for a key size of 128, 192, or 256 bits.");
	}

	var counterLow = 0, counterHigh = 0; // 32 bits each
	var res = [];
	while (data.length) {
		var counterBuffer = new Buffer(16);
		counterBuffer.fill(0);
		counterBuffer.writeUInt32LE(counterLow, 8);
		counterBuffer.writeUInt32LE(counterHigh, 12);

		var ciphertext = aes.encrypt(counterBuffer, key);
		ciphertext = Array.prototype.slice.call(ciphertext);

		var plaintext = data.splice(0, key.length);
		plaintext.forEach(function(byte, index) {
			plaintext[index] ^= ciphertext[index];
		});

		res.splice.bind(res, 0, 0).apply(null, plaintext); // JS magic
		counterLow++;
		if (counterLow == 0xFFFFFFFF) {
			counterHigh++;
			counterLow = 0;
		}
	}

	res = new Buffer(res);
	return res;
}
