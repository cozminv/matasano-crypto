'use strict';

var keySchedule = require("./keySchedule");
var tables = require("./tables");

module.exports = {
	encrypt : encrypt,
	decrypt : decrypt
};

function generateMatrix(data) {
	// column major order
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][j] = data[4 * i + j];

	return res;
}

function generateBuffer(data) {
	// column major order
	var res = new Buffer(16);

	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[4 * i + j] = data[i][j];

	return res;
}

function addRoundKey(data, key) {
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][j] = data[i][j] ^ key[i][j];

	return res;
}

function subBytes(data) {
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][j] = tables.s[data[i][j]];

	return res;
}

function invSubBytes(data) {
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][j] = tables.inv_s[data[i][j]];

	return res;
}

function shiftRows(data) {
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][(j - i + 4) % 4] = data[i][j];

	return res;
}

function invShiftRows(data) {
	var res = [ [], [], [], [] ];
	for (var i = 0; i < 4; ++i)
		for (var j = 0; j < 4; ++j)
			res[i][(j + i) % 4] = data[i][j];

	return res;
}

function mixColumns(data) {
	var res = [ [], [], [], [] ];
	for (var j = 0; j < 4; ++j) {
		var a0 = data[0][j];
		var a1 = data[1][j];
		var a2 = data[2][j];
		var a3 = data[3][j];

		res[0][j] = tables.mix_2[a0] ^ tables.mix_3[a1] ^ a2 ^ a3;
		res[1][j] = a0 ^ tables.mix_2[a1] ^ tables.mix_3[a2] ^ a3;
		res[2][j] = a0 ^ a1 ^ tables.mix_2[a2] ^ tables.mix_3[a3];
		res[3][j] = tables.mix_3[a0] ^ a1 ^ a2 ^ tables.mix_2[a3];
	}

	return res;
}

function invMixColumns(data) {
	var res = [ [], [], [], [] ];
	for (var j = 0; j < 4; ++j) {
		var a0 = data[0][j];
		var a1 = data[1][j];
		var a2 = data[2][j];
		var a3 = data[3][j];

		res[0][j] = tables.mix_14[a0] ^ tables.mix_11[a1] ^ tables.mix_13[a2] ^ tables.mix_9[a3];
		res[1][j] = tables.mix_9[a0] ^ tables.mix_14[a1] ^ tables.mix_11[a2] ^ tables.mix_13[a3];
		res[2][j] = tables.mix_13[a0] ^ tables.mix_9[a1] ^ tables.mix_14[a2] ^ tables.mix_11[a3];
		res[3][j] = tables.mix_11[a0] ^ tables.mix_13[a1] ^ tables.mix_9[a2] ^ tables.mix_14[a3];
	}
	
	return res;
}

function doRound(data, key, isFinal) {
	data = subBytes(data);
	data = shiftRows(data);
	if (!isFinal)
		data = mixColumns(data);
	data = addRoundKey(data, key);

	return data;
}

function doInverseRound(data, key, isFinal) {
	data = addRoundKey(data, key);
	if (!isFinal)
		data = invMixColumns(data);
	data = invShiftRows(data);
	data = invSubBytes(data);

	return data;
}

function encrypt(data, key) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof data === 'string')
		data = new Buffer(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	if (data.length !== 16)
		throw new TypeError("AES only works for a block size of 128 bits.");

	switch (key.length) {
	case 16:
		break;
	case 24:
	case 32:
		throw new TypeError(key.length + " bit key support not implemented.");
	default:
		throw new TypeError("AES only works for a key size of 128, 192, or 256 bits.");
	}

	var dataMatrix, currentKey, keyMatrix;

	// create data matrix
	dataMatrix = generateMatrix(data);

	// generate key expansions
	var keyExpansions = keySchedule.generateKeys(key);

	// initial round
	currentKey = keyExpansions.shift();
	keyMatrix = generateMatrix(currentKey);
	dataMatrix = addRoundKey(dataMatrix, keyMatrix);

	// remaining rounds
	while (keyExpansions.length) {
		var isFinal = (keyExpansions.length == 1);
		currentKey = keyExpansions.shift();
		keyMatrix = generateMatrix(currentKey);
		dataMatrix = doRound(dataMatrix, keyMatrix, isFinal);
	}

	var result = generateBuffer(dataMatrix);
	return result;
}

function decrypt(data, key) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof data === 'string')
		data = new Buffer(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	if (data.length !== 16)
		throw new TypeError("AES only works for a block size of 128 bits.");

	switch (key.length) {
	case 16:
		break;
	case 24:
	case 32:
		throw new TypeError(key.length + " bit key support not implemented.");
	default:
		throw new TypeError("AES only works for a key size of 128, 192, or 256 bits.");
	}

	var dataMatrix, currentKey, keyMatrix;

	// create data matrix
	dataMatrix = generateMatrix(data);

	// generate key expansions
	var keyExpansions = keySchedule.generateKeys(key);

	// remaining rounds
	var isFinal = true;
	while (keyExpansions.length != 1) {
		currentKey = keyExpansions.pop();
		keyMatrix = generateMatrix(currentKey);
		dataMatrix = doInverseRound(dataMatrix, keyMatrix, isFinal);
		isFinal = false;
	}

	// initial round
	currentKey = keyExpansions.pop();
	keyMatrix = generateMatrix(currentKey);
	dataMatrix = addRoundKey(dataMatrix, keyMatrix);

	var result = generateBuffer(dataMatrix);
	return result;
}
