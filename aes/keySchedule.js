'use strict';

var tables = require("./tables");

module.exports = {
	generateKeys : generateKeys
};

function keyScheduleCore(input, i) {
	var res = [];

	for (var i = 0; i < 4; ++i)
		res[(i - 1 + 4) % 4] = input[i];

	for (var i = 0; i < 4; ++i)
		res[i] = tables.s[res[i]];
	res[0] ^= tables.rcon[i];
	return res;
}

function generateKeys(key) {
	var expandedKey = [];
	var n = key.length;

	for (var i = 0; i < n; ++i)
		expandedKey.push(key[i]);

	for (var i = 1; i <= tables.rounds[n]; ++i) {
		var t, block;
		t = expandedKey.slice(-4);
		t = keyScheduleCore(t, i);

		block = expandedKey.slice(-n, -n + 4);
		t.map(function(byte, index) {
			return byte ^= block[index];
		});
		t.forEach(function(e) {
			expandedKey.push(e);
		});

		for (var j = 0; j < 3; ++j) {
			block = expandedKey.slice(-n, -n + 4);
			t.map(function(byte, index) {
				return byte ^= block[index];
			});
			t.forEach(function(e) {
				expandedKey.push(e);
			});
		}
	}

	var keys = [];
	for (var i = 0; i <= tables.rounds[n]; ++i) {
		var oneKey = expandedKey.slice(16 * i, 16 * (i + 1));
		keys.push(new Buffer(oneKey)); // XXX buffer not needed
	}

	return keys;
}
