'use strict';

module.exports = {
	encryptRepeatingKeyXOR : encryptRepeatingKeyXOR
};

function encryptRepeatingKeyXOR(data, key) {
	if (typeof data !== 'string' && !(data instanceof Buffer))
		throw new TypeError("The data should be a string or a Buffer.");

	if (typeof key !== 'string' && !(key instanceof Buffer))
		throw new TypeError("The key should be a string or a Buffer.");

	if (typeof data === 'string')
		data = new Buffer(data);

	if (typeof key === 'string')
		key = new Buffer(key);

	var result = new Buffer(data.length);
	for (var i = 0, j = 0; i < data.length; ++i, j = ++j % 3) {
		result[i] = data[i] ^ key[j];
	}

	return result;
}
