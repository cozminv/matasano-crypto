'use strict';

var assert = require('assert');

var s1c5 = require("./s1c5");
var s3c18 = require("./s3c18");

suite("Matasano", function() {
	suite("Set 1", function() {
		suite("Challange 5", function() {
			test("example check", function() {
				var data = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
				var key = "ICE";
				var result = s1c5.encryptRepeatingKeyXOR(data, key).toString('hex');
				var expected = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272"
						+ "a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
				assert.equal(result, expected);
			});
		});
	});
	suite("Set 3", function() {
		suite("Challance 18", function() {
			test("example check", function() {
				var data = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";
				var key = "YELLOW SUBMARINE";
				var result = s3c18.AES_CTR(data, key).toString('ascii');
				var expected = ""; // TODO
				assert.equal(result, expected);
			});
		});
	});
});
